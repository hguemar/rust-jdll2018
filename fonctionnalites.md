# Rust et la sûreté


## Commençons par un exemple anodin

```rust
let x = 5;
x = 4;
```


## Immutabilité par défaut

```bash
error[E0384]: cannot assign twice to immutable variable `x`
--> test-mut-1.rs:3:5
|
2 |     let x = 5;
|         - first assignment to `x`
3 |     x = 4;
|     ^^^^^ cannot assign twice to immutable variable
```


## To mut or not to mut

```bash
let mut x = 5;
x = 4;
```


## Ownership pour les nuls

* Pas de garbage collector, pas de destructeurs explicites
* La durée de vie d'un objet est lié à son scope
* Rust impose 1 ressource = 1 propriétaire


## Move

```rust
let x1 = 5;
let x2 = x1;

println!("x1 = {}", x1); // erreur x1 a été invalidé!

```


## Références et borrowing

Rust permet d'emprunter une variable
* Soit de manière mutable mais une seule fois!
* Soit immutable (plusieurs fois si on veut)

```rust
let mut x = 5;
let y = &mut x; // référence mutable
```


## Concurrence

Par défaut, Rust est thread-safe

```rust
  let x = 1;

  thread::spawn(|| {
    println!("Valeur de x: {})", x);
  });
}

```


## Concurrence

```bash
error[E0373]: closure may outlive the current function, but it borrows `x`, which is owned by the current function
 --> test-thread-1.rs:6:17
  |
6 |   thread::spawn(|| {
  |                 ^^ may outlive borrowed value `x`
7 |     println!("Valeur de x: {})", x);
  |                                  - `x` is borrowed here
help: to force the closure to take ownership of `x` (and any other referenced variables), use the `move` keyword
  |
6 |   thread::spawn(move || {
  |                 ^^^^^^^

error: aborting due to previous error
```


## Concurrence

On corrige la closure en déplaçant les variables

```rust
  let x = 1;

  thread::spawn(move || {
    println!("Valeur de x: {})", x);
  });
```


## concurrence

Pour afficher le message, on attends que le thread soit exécuté!

```Rust
  let x = 1;

  let handle = thread::spawn(move || {
    println!("Valeur de x: {})", x);
  });

  handle.join();
```


## Une histoire de types biens et de types louches

Rust définit deux traits en rapport à la concurrence

11 possibilités, soit votre type T implémente:
- . Sync => type totalement sûr
- . Send => type dont l'ownership peut être transféré de manière sûre
- . Ni l'un, ni l'autre (ex: bibliothèque native)
