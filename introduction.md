# Introduction à Rust

<img src="./images/logo.svg" style="margin-top: 50px; transform: scale(1.5); enable-background:new 0 0 144 144; background-color: white;">


## Présentations

- . Florent
- . Haïkel

- . On organise les TupperRust (travaux pratiques autour de Rust)
  - https://tupperrust.github.io
  - Compte twitter : [@TupperRust](https://twitter.com/TupperRust)
- . Pas des experts (mais on se soigne!)


## Pour qui ?

 - . Débutant•e•s / curieu•ses•x / en Rust (bienvenue ! \o/)
 - . Que vous ayez pratiqué ou jamais
 - . Ou si vous souhaitez un rappel des bases



## Rust : Késako ?

Langage de programmation natif initié par Graydon Hoare au sein de Mozilla

 - . Né officiellement en 2009, publié en 2010
 - . Version 1.0 lancée en 2015
 - . Développé de façon ouverte, la majorité des contributions vient maintenant de la communauté.


## Rust : Késako ?

> Langage de programmation ultra-rapide, prévient les erreurs de segmentation et garantit la sûreté entre les threads.

*source : rust-lang.org*

On peut rajouter que c'est un langage de programmation orienté système (du moins initialement ;)).


## Rust : Pourquoi ?

 - . un langage de programmation sûr pour les applications concurrentes
 - . Faire du bas-niveau sans crainte
   - éviter beaucoup de pièges (*use after free*, accès à un pointeur *null*, …)
 - . être simple d'utilisation
 - . des erreurs de compilation claires pour les débutants
 - . …
 - . le tout sans sacrifier la vitesse d'exécution


## La promesse est-elle tenue ?

 - . Pour la sureté du langage : Oui !
 - . Pour la facilité : Ça reste plus dure que Python mais bien plus facile que Haskell
 - . Pour les erreurs claires et l'assistance à leur résolution : ça progresse bien !


## Exemple d'erreur en Rust ?

<img src="images/exemple_erreur.jpg">

- . Malgré ça, besoin de plus de détails ? <br>→ https://doc.rust-lang.org/error-index.html#E0425


## Dessine-nous un hello world !

```rust
fn main() {
    println!("Hello world");
}
```


## Un exemple plus développé

```rust
fn sum(a: u32, b: u32) -> u32 {
    // absence de `;` équivalent à `return a + b;`
    a + b
}

fn main() {
    // Déclaration de variables :
    let a: u32 = 3; // Typage explicite
    let b = 4;       // Typage inféré \o/
    let c = vec![1, 2, 3]; // Aussi avec les types composés !
    println!("sum of {} + {} = {}", a, b, sum(a, b));
}
```


## On veut voir !

→ OK ! Voyons comment on monte en place l'environnement !
