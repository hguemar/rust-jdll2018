# Quelques projets stars !


## Servo

<img src="images/servo.png" >


## Servo

- Moteur de rendu expérimental de Mozilla écrit en Rust
- Apporte le parallélisme dans beaucoup de composants : le rendu, la disposition d'éléments, le parsing, …

→ https://servo.org


## Firefox Quantum !

<img src="images/quantum.jpg">


## Redox-OS

<img src="images/redox.png">

- Système d'exploitation avec un micro-kernel écrit en Rust.
- Mais aussi plein de projets autour (le shell ion, le gestionnaire de bureau Orbital, le système de fichier TFS, …)

→ https://redox-os.org


## Des alternatives à coreutils

- . Ripgrep : grep sous stéroïde !
- . fd-find : find pour les humains
  - coreutils : `find -iname "*PATTERN*`
  - avec fd : `fd PATTERN`
- . exa : un remplacement de ls avec plein d'options sympathique


## Rayon : Faire du parallélisme de données

 - bibliothèque pour faire du traitement parallèle de données
 - `s/.iter()/.par_iter()/g` → TADA !

 ```rust
 fn sum_of_squares(input: &[i32]) -> i32 {
   input.par_iter()
     .map(|&i| i * i)
     .sum()
 }
```


## Nom : un framework de parsing

 - Framework de parsing combinateur
 - orienté bits / octets / strings
 - Rapide
 - sans copie
 - utilise des macros


## Cargo-watch

 - une extension de cargo pour exécuter une commande à chaque changement sur votre projet

 - Exemple : réexécuter la batterie de test à chaque modification des sources / tests :
```bash
$ cargo watch -x test
```


## WebAssembly avec Rust

 - . WebAssembly : Standard web de Bytecode exécutable par le navigateur
 - . Dans la roadmap cette année : faciliter la réalisation d'application Rust pour le Web avec WebAssembly
  - bindings JS ←→ Rust


## Gros projets libres

 - . GNOME a introduit Rust dans librsvg
 - . Gstreamer réécrit certaines extensions en Rust
 - . Le backend de npm utilise Rust pour les métriques


## Et plein d'autres !

 - . La liste n'est pas exhaustive !
 - . Peut-être le vôtre ? :D
